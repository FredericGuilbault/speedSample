const yargs = require('yargs');
const express = require('express')

/* ==== Web Server ==== */
var app = express();
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use(express.static(__dirname + '/static'));

app.set("twig options", {
    allow_async: true,
    strict_variables: false
});
/* ==== CLI arguments ==== */
const argv = yargs
    .options({
        'host': {
            alias: 'H',
            describe: 'host of signalk server. \n (default: 127.0.0.1)\n'
        },
        'port': {
            alias: 'P',
            describe: 'Port of singalk server. \n (default: 3000)\n'
        },
        'webport': {
            alias: 'w',
            describe: 'Port witch this app should serve on. \n (default: 4998)\n'
        },
        'subscription_speed': {
          alias: 'p',
          describe: 'Data subscription on signalkServer \n (default: navigation.speedOverGround)\n'
        },
        'subscription_bearing': {
          alias: 'b',
          describe: 'Data subscription on signalkServer \n (default: navigation.​courseOverGroundMagnetic)\n'
        }
    })
    .help()
    .argv;

if( !argv.host ){
    argv.host = '127.0.0.1'
    argv.host = 'localhost'
    console.log('Using default value for host: '+argv.host )
}

if( !argv.port ){
    argv.port = '3000'
    console.log('Using default value for port: '+argv.port )
}

if( !argv.webport ){
    argv.webport= '4998'
}

if( !argv.subscription_speed ){
    argv.subscription_speed= 'navigation.speedOverGround'
}

if( !argv.subscription_bearing ){
    argv.subscription_bearing = 'navigation.​courseOverGroundMagnetic'
}

/* webserver */
app.get('/', function(req, res){
    res.render('index.twig',{
        host: argv.host,
        port: argv.port,
        subscriptionPath_Speed: argv.subscription_speed,
        subscriptionPath_Bearing: argv.subscription_bearing
    });
});

app.listen(argv.webport, () => console.log('WebServer is Listening on port '+argv.webport))
