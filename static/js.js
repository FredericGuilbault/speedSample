const startbtn = document.getElementById('startTimer')
var collectedData = [];

startbtn.onclick = function(){
    askAsample(function(avg) {
        var date =  new Date()
        var table = document.getElementById("resultTable")
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.insertCell(0).innerHTML= date.getHours()+':'+date.getMinutes();
        //row.insertCell(1).innerHTML=   '---'
        row.insertCell(1).innerHTML=   avg.toFixed(2)
    })
};

function askAsample(callback) {
    var ws = new WebSocket((window.location.protocol === 'https:' ? 'wss' : 'ws') + "://" +signalkHost + "/signalk/v1/stream?subscribe=none");
    ws.onopen = function() {
        var subscriptionObject = {
            "context": "vessels.self",
            "subscribe": [{
                "path": subscriptionPath_Speed,
                "policy": "instant"
            }
          ]
        };
        var subscriptionMessage = JSON.stringify(subscriptionObject);
        console.log("Sending subscription:" + subscriptionMessage)
        collectedData = [];

        var elmt = document.getElementById("startTimer");
        elmt.style.visibility = "hidden";
        elmt = document.getElementById("progressBar");
        elmt.style.visibility =  "visible";

        ws.send(subscriptionMessage);

        var progressBar =  setInterval(function() {
            var width = document.getElementsByClassName("progress-bar")[0].style.width
            var width = parseInt( width.replace('%',''), 10) + 1;
            document.getElementsByClassName("progress-bar")[0].setAttribute("style", "width:"+width+"%");
            document.getElementsByClassName("progress-bar")[0].style.width = width+"%";
        },600)

        setTimeout(function() {
          function getSum(total, num) {
            return total + num;
          }

          var sum =  collectedData.reduce(getSum, 0)
          var avg = sum / collectedData.length
          var elmt = document.getElementById("startTimer");
          elmt.style.visibility =  "visible";
          elmt = document.getElementById("progressBar");
          elmt.style.visibility = "hidden";
          ws.close()
          clearTimeout(progressBar)
          document.getElementsByClassName("progress-bar")[0].setAttribute("style", "width:"+0+"%");
          document.getElementsByClassName("progress-bar")[0].style.width = 0+"%";
          callback(avg)
        },60000)

    }

    ws.onclose = function() {
        console.log("ws closed");
        collectedData = [];
    }

    ws.onerror = function(e) {
      elmt = document.getElementById("error-msg");
      elmt.style.visibility =  "visible";
      elmt.innerHTML = 'Connection Error';
      setTimeout(function(){
        elmt = document.getElementById("error-msg");
        elmt.style.visibility =  "hidden";
      },2000)

    }

    ws.onmessage = function(event) {
        try {
          var sog = JSON.parse(event.data).updates[0].values[0].value
        }catch(err) {
          console.log('invalid data');
          return
        }
        collectedData.push(sog)
    }


}
